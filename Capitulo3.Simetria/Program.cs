﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capitulo3.Simetria
{
    
    class Program
    {
        public static int EntradaValor;
        public static int EntradaResultado;
        public static void Entrada()
        {
            int resp = 0;
            Console.Write("Ingrese numero: ");
            resp = Convert.ToInt32(Console.ReadLine());
            EntradaValor = resp;
        }

        public static void Proceso()
        {
            EntradaResultado = EntradaValor * EntradaValor;
        }

        public static void Salida()
        {
            Console.WriteLine("resp es "+ EntradaResultado);
            Console.ReadKey();
        }
        //En las llamadas metodos abajo esta la simetria
        static void Main(string[] args)
        {
            Entrada();
            Proceso();
            Salida();
        }
    }
}
